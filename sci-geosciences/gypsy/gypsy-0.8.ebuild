# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="Gypsy is a GPS multiplexing daemon."
HOMEPAGE="http://gypsy.freedesktop.org/wiki/"
SRC_URI="http://gypsy.freedesktop.org/releases/${P}.tar.gz"

LICENSE="GPL-2 LGPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="bluetooth"

RDEPEND="sys-apps/dbus
	dev-libs/glib:2
	bluetooth? ( net-wireless/bluez )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

src_configure() {
	econf $(use_enable bluetooth )
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
}
