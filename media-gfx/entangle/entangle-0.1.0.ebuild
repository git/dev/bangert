# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit eutils autotools

DESCRIPTION="Tethered Camera Control & Capture for Nikon and Canon DSLRs"
HOMEPAGE="http://capa-project.org/"
SRC_URI="http://entangle-photo.org/download/sources/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="x11-libs/gtk+:2
	media-libs/libgphoto2
	dev-libs/libunique
	media-libs/lcms
	gnome-base/libglade:2.0
	dev-libs/glib:2
	gnome-base/gconf
	dev-libs/dbus-glib
	dev-libs/gjs
	dev-libs/gir-repository"
DEPEND="dev-util/pkgconfig
	dev-perl/Template-Toolkit
	${RDEPEND}"

src_prepare() {
	epatch "${FILESDIR}"/${P}-fix-install.diff
	epatch "${FILESDIR}"/${P}-fix-segfault-f3d9b75d68.patch
	eautoreconf
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
	dodoc README NEWS ChangeLog || die
}
