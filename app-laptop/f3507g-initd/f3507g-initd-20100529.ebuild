# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="control scripts for f3507g"
HOMEPAGE="http://www.gentoo.org/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="gps"

DEPEND=""
RDEPEND="${DEPEND}
	net-dialup/ppp
	gps? ( >=sci-geosciences/gpsd-2.39 )"

src_install() {
	newconfd "${FILESDIR}/f3507g.confd" f3507g
	newinitd "${FILESDIR}/f3507g.initd" f3507g
	use gps && newinitd "${FILESDIR}/f3507g-gpsd.initd" f3507g-gpsd
	use gps && newconfd "${FILESDIR}/f3507g-gpsd.confd" f3507g-gpsd
	dodoc "${FILESDIR}"/net.example
}

pkg_postinst() {
	elog "Use the /etc/conf.d/f3507g-gpsd script to start a gpsd"
	elog "instance which uses the f3507g gps device."
	elog "An ppp example config has been installed at"
	elog "/usr/share/doc/${PV}"
}
