#!/usr/bin/ruby
#
#
#

BASE_DIR = "/home/bangert/gentoo/gentoo/"

require "rexml/document"

userxml = REXML::Document.new File.new( BASE_DIR + "xml/htdocs/proj/en/devrel/roll-call/userinfo.xml" )
developers = Hash::new(false)
userxml.elements.each("userlist/user") { |e|
	dev = e.attributes["username"].strip.downcase
	if e.elements["status"] && e.elements["status"].text == "retired"
		developers[dev] = false
	else
		developers[dev] = true
	end
}

# takes a REXML::Document and checks project/dev elements against second
# argument hash
#
class ProjectChecker
	def self.checkDevs rexml, developers, prefix
		nr_of_devs = 0
		rexml.elements.each("project/dev") { |dev|
			developer = dev.text.strip.downcase
			if !developers.has_key?(developer)
				puts prefix + ": Unknown developer: #{developer}"
			elsif !developers[developer]
			        puts prefix + ": Retired devloper: #{developer}"
			else
				nr_of_devs += 1
			end
		}
		if nr_of_devs == 0
			puts prefix + ": Project DEAD! Zero developers signed up."
		elsif nr_of_devs < 2
			puts prefix + ": Only #{nr_of_devs} developers signed up for project!"
		end
	end

	def self.recursiveFullCheck developers 
		self.recursiveFullCheckWorker BASE_DIR + "xml/htdocs/proj/en/metastructure/gentoo.xml" ,developers
	end

	def self.recursiveFullCheckWorker xmlfile, developers
		projectxml = REXML::Document.new File.new( xmlfile )
		name = projectxml.elements["project/name"].text.strip
		
		ProjectChecker.checkDevs(projectxml, developers, name)
		projectxml.elements.each("project/subproject") { |project|
			projectfile = BASE_DIR + "xml/htdocs" +  project.attribute("ref").to_s
			if (File.exists? projectfile) 
				self.recursiveFullCheckWorker projectfile, developers
			else
				#puts "ERROR: File not found: " + projectfile 
				puts "Project \"#{name}\" does not habe this subproject reference: " + project.attribute("ref").to_s
			end
		}
	end
end

ProjectChecker.recursiveFullCheck developers

