#!/usr/bin/ruby

#TODO
# - multiple maintainer tags in metadata.xml

require "rexml/document"

PORTAGE_DIR="/usr/portage/"
#PORTAGE_DIR="/home/bangert/gentoo/test-portage"

herdxml = REXML::Document.new File.new( "/home/bangert/gentoo/gentoo/xml/htdocs/proj/en/metastructure/herds/herds.xml" )

herds = Hash::new(false)
herdemail = Hash::new(false)

herdxml.elements.each("herds/herd") { |e|
        herds[e.elements["name"].text] = true
	email = e.elements["email"]
	if !email.nil?
        	herdemail[email.text.split("@")[0]] = true
	else
		puts "herd without email: #{e.elements["name"].text}"
	end
}

userxml = REXML::Document.new File.new( "/home/bangert/gentoo/gentoo/xml/htdocs/proj/en/devrel/roll-call/userinfo.xml" )

developers = Hash::new(false)
userxml.elements.each("userlist/user") { |e|  
	if e.elements["status"] && e.elements["status"].text == "retired"
		developers[e.attributes["username"]] = false
	else
		developers[e.attributes["username"]] = true 
	end

}

ct_herd_unknown_member = 0
ct_herd_retired_member = 0
ct_herd_nomembers = 0

herdxml.elements.each("herds/herd") { |e|
	if e.elements["maintainer"]
		e.elements.each("maintainer/email") { |m|
			email = m.text.downcase.strip.split("@")
			if email[1] != "gentoo.org"
				puts "error"
			else
				if !developers.has_key?(email[0])
					ct_herd_unknown_member += 1
					puts e.elements['name'].text.ljust(20) + "unknown member: #{email[0]}"
				elsif !developers[email[0]]
					ct_herd_retired_member+=1
					puts e.elements['name'].text.ljust(20) + "retired member: #{email[0]}"
				end
			end
		}
	else
		ct_herd_nomembers += 1
		puts e.elements['name'].text.ljust(20) + "no members"
	end
}

