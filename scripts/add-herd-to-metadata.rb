#!/usr/bin/ruby

require "rexml/document"
PORTAGE_DIR="/home/bangert/gentoo/gentoo-x86"
#PORTAGE_DIR="/home/bangert/gentoo/test-portage"
count = 0
category = nil
IO::foreach("add-herd-package-list") { |package|
	package = package.strip
	curcat = package.split("/")[0]
#	if category.nil?
#		category = curcat
	#elsif count > 5 && category != curcat
#	elsif category != curcat
#		puts "Done with category #{category} - next category #{curcat}"
#		break
#	end
	Dir::chdir("#{PORTAGE_DIR}/#{package}") do 
		outdata = ""
		something_changed = false
		IO::foreach("metadata.xml") { |line|
			if line == "<pkgmetadata>\n"
				line = "<pkgmetadata>\n<herd>no-herd</herd>\n"
				something_changed = true
			end 
			outdata += line
		}
		if something_changed
			f = File::open("metadata.xml","w")
			f.write(outdata)
			f.close
			system("cvs diff metadata.xml")
		end
		system("echangelog 'add <herd>no-herd</herd>'")
		system("ebuild #{Dir["*.ebuild"][0]} manifest")


	end
	count += 1
}
puts "#{count} Packages fixed!"


