#!/bin/bash

die() {
	echo $1
	exit
}

msg() {
	echo
	echo "*** "$1
	echo
}

[ "0" -ne "`id -u`" ] && die "You must be root!"

USBDISK=$1
ISO=$2

[ -z "${USBDISK}" ] && die "No USBdisk given!"
[ -z "${ISO}" ] && die "No ISO given!"
[ -e ${USBDISK} ] || die "USBdisk not found: ${USBDISK}"
[ -e ${ISO} ] || die "ISO not found: ${ISO}"

MNTPTLOOP=`mktemp -d /mnt/tmp.usb2iso.XXXXXXXXXX`
MNTPTUSB=`mktemp -d /mnt/tmp.usb2iso.XXXXXXXXXX`

msg "Creating filesystem!"
mke2fs ${USBDISK} || die "Creating filesystem failed!"

msg "Mounting USBdisk!"
mount ${USBDISK} ${MNTPTUSB} || die "Could not mount USBdisk: ${USBDISK}"

msg "Mounting ISO!"
mount -o loop ${ISO} ${MNTPTLOOP} || die "Could not mount ISO: ${ISO}"

msg "Installing bootloader!"
grub-install --no-floppy --root-directory=${MNTPTUSB} ${USBDISK}

msg "Copying system files!"
cp ${MNTPTLOOP}/isolinux/gentoo     ${MNTPTUSB}/boot/
cp ${MNTPTLOOP}/isolinux/gentoo.igz ${MNTPTUSB}/boot/

cp ${MNTPTLOOP}/livecd              ${MNTPTUSB}
cp ${MNTPTLOOP}/Getting_Online.txt  ${MNTPTUSB}
cp ${MNTPTLOOP}/README.txt          ${MNTPTUSB}
cp ${MNTPTLOOP}/image.squashfs      ${MNTPTUSB}
cp ${MNTPTLOOP}/gentoo.efimg        ${MNTPTUSB}

msg "Installing bootloader configuration!"
cat > ${MNTPTUSB}/boot/grub/menu.lst << EOF
title           Gentoo Linux
root            (hd1,0)
kernel          /boot/gentoo root=/dev/ram0 init=/linuxrc dokeymap looptype=squashfs loop=/image.squashfs  cdroot initrd=gentoo.igz quiet
initrd          /boot/gentoo.igz
boot
EOF

msg "Cleaning up! Unmounting Loop!"
umount ${MNTPTLOOP}
msg "Cleaning up! Unmounting USB!"
umount ${MNTPTUSB}

rmdir ${MNTPTLOOP}
rmdir ${MNTPTUSB}
msg "Success!"

