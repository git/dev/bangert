#!/usr/bin/ruby

#TODO
# - multiple maintainer tags in metadata.xml

require "rexml/document"

PORTAGE_DIR="/usr/portage/"
#PORTAGE_DIR="/home/bangert/gentoo/test-portage"

herdxml = REXML::Document.new File.new( "/home/bangert/gentoo/gentoo/xml/htdocs/proj/en/metastructure/herds/herds.xml" )

herds = Hash::new(false)
herdemail = Hash::new(false)

herdxml.elements.each("herds/herd") { |e|
        herds[e.elements["name"].text] = true
	email = e.elements["email"]
	if !email.nil?
        	herdemail[email.text.split("@")[0]] = true
	else
		puts "herd without email: #{e.elements["name"].text}"
	end
}

userxml = REXML::Document.new File.new( "/home/bangert/gentoo/gentoo/xml/htdocs/proj/en/devrel/roll-call/userinfo.xml" )

developers = Hash::new(false)
userxml.elements.each("userlist/user") { |e|  
	if e.elements["status"] && e.elements["status"].text == "retired"
		developers[e.attributes["username"]] = false
	else
		developers[e.attributes["username"]] = true 
	end

}
#developers.each{ |x,y|
#	puts "#{x} #{y}"
#}

#herdxml.each_element_with_text("rox") {|e| p e.text}
categories=`cat #{PORTAGE_DIR}/profiles/categories`.split()

ct_packages = 0
ct_retired_maint = 0
ct_unknown_maint = 0
ct_invalid_proxy_maint = 0
ct_missing_herd = 0
ct_missing_maint = 0
ct_empty_herd = 0
ct_unknown_herd = 0
ct_missing_metadata = 0
ct_unmaintained_package = 0
ct_maint_needed_no_no_herd = 0
ct_maint_herd = 0
ct_no_herd = 0
ct_ldesc_empty = 0
ct_ldesc_lang = 0


categories.map{ |x|
	puts "Category: #{x}" 
	
	`ls -1 #{PORTAGE_DIR}/#{x}`.split().map{ |package|
		if package != "CVS" && package != "metadata.xml"
			ct_packages += 1
			metadatafile = "#{PORTAGE_DIR}/#{x}/#{package}/metadata.xml"
			if File::file? metadatafile
				metadata = REXML::Document.new File.new( metadatafile )
				proxy_maint = false
				gentoo_maint = false
				herds = Hash::new(false)
				if metadata.elements["pkgmetadata/herd"]
					metadata.elements.each("pkgmetadata/herd") { |herd|
						herd = herd.text
						if herd.nil?
							ct_empty_herd += 1
							puts "#{x}/#{package}".ljust(40) + "<herd> empty"
						else
							herd = herd.strip
							herds[herd] = true
							if herd == "no-herd"
								ct_no_herd += 1
							else
								if herds[herd]
									gentoo_maint = true
								else
									puts "#{x}/#{package}".ljust(40) + "<herd> unknown: <herd>#{herd}</herd>"
									ct_unknown_herd += 1
								end
							end
						end
					}
				elsif
					ct_missing_herd += 1
					puts "#{x}/#{package}".ljust(40) + "<herd> missing"
				end
				if metadata.elements["pkgmetadata/maintainer"]
					metadata.elements.each("pkgmetadata/maintainer/email") { |m| 
						email = m.text.downcase.strip.split("@")
						if email[1] != "gentoo.org"
							proxy_maint = true
							#puts "#{x}/#{package}".ljust(40) + "non-gentoo maintainer: #{m.text}"
						else
							if !developers.has_key?(email[0])
								if email[0] == "maintainer-needed" 
									ct_maint_needed_no_no_herd += 1
								elsif herdemail[email[0]]
									ct_maint_herd += 1
									puts "#{x}/#{package}".ljust(40) + "herd as maintainer: #{m.text}"
								else
									ct_unknown_maint+=1
									puts "#{x}/#{package}".ljust(40) + "unknown maintainer: #{m.text}"
								end
							elsif !developers[email[0]]
								ct_retired_maint+=1
								puts "#{x}/#{package}".ljust(40) + "retired maintainer: #{m.text}"
							else
								gentoo_maint = true
							end
						end
					}
				else
					ct_missing_maint += 1
			#		puts "#{x}/#{package}".ljust(40) + "missing <maintainer>"
				end
#				if metadata.elements["pkgmetadata/longdescription"]
#					metadata.elements.each("pkgmetadata/longdescription") { |d|
#						ldesc = d.text
#						if ldesc.nil?
#							ct_ldesc_empty += 1
#							puts "#{x}/#{package}".ljust(40) + "<longdescription> empty"
#						end
#						if !d.attributes["lang"]
#							ct_ldesc_lang += 1
#							puts "#{x}/#{package}".ljust(40) + "<longdescription> missing lang attribute"
#						end
#					}
#				end
				if !gentoo_maint
					if proxy_maint
						ct_invalid_proxy_maint += 1
						puts "#{x}/#{package}".ljust(40) + "has proxy maintainer but no gentoo association"
					else
						ct_unmaintained_package += 1
						puts "#{x}/#{package}".ljust(40) + "unmaintained"
					end
				end
				
			elsif
				ct_missing_metadata += 1
				puts "#{x}/#{package}".ljust(40) + "metadata.xml missing"
			end
		end
	}

}

puts "\nStatistics"
puts "=================================================================="
puts
puts "Total number of packages:".ljust(40) + "#{ct_packages}".rjust(10)
puts
puts "metadata.xml missing".ljust(40) + "#{ct_missing_metadata}".rjust(10)
puts "<herd> missing".ljust(40) + "#{ct_missing_herd}".rjust(10)
puts "<herd> empty".ljust(40) + "#{ct_empty_herd}".rjust(10)
puts "<herd> unknown".ljust(40) + "#{ct_unknown_herd}".rjust(10)
puts "<herd>=no-herd".ljust(40) + "#{ct_no_herd}".rjust(10)
puts
puts "<maintainer> missing".ljust(40) + "#{ct_missing_maint}".rjust(10)
puts "<maintainer> retired".ljust(40) + "#{ct_retired_maint}".rjust(10)
puts "<maintainer> is a herd".ljust(40) + "#{ct_maint_herd}".rjust(10)
puts "<maintainer> unknown".ljust(40) + "#{ct_unknown_maint}".rjust(10)
puts "<maintainer>=maintainer-needed".ljust(40) + " #{ct_maint_needed_no_no_herd}".rjust(10)
puts
puts "Proxy maintainer without gentoo association".ljust(40) + "#{ct_invalid_proxy_maint}".rjust(10)
puts "Unmaintained packages".ljust(40) +"#{ct_unmaintained_package}".rjust(10)
puts
#puts "<longdescription> empty".ljust(40) + "#{ct_ldesc_empty}".rjust(10)
#puts "<longdescription> missing lang attribute".ljust(40) + "#{ct_ldesc_lang}".rjust(10)
