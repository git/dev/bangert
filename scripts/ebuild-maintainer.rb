#!/usr/bin/ruby

#SEARCH=$1
#RESULT=`qsearch -NC ${SEARCH}`

#if [ "${RESULT}" != "" ]; then
#  for x in ${RESULT};
#  do
#    echo ${x}
#    cat /usr/portage/${x}/metadata.xml
#  done
#fi

require "rexml/document"


herdxml = REXML::Document.new File.new( "/var/lib/herdstat/herds.xml" )
#herdxml.each_element_with_text("rox") {|e| p e.text}
search=$*.pop
result=`qsearch -NC #{search}`.split()

result.map{ |x|
	#puts x
	hit = x
	metadata = REXML::Document.new File.new( "/usr/portage/#{x}/metadata.xml" )
	if metadata.elements["pkgmetadata/herd"]
		herd = metadata.elements["pkgmetadata/herd"].text
	end
	hit += " ("
	if herd != "no-herd"
		if !herd.nil?
			herdxml.elements.each("herds/herd") { |e|  
				if ( e.elements["name"].text==herd && e.elements["email"].has_text? ) then 
					hit += e.elements["email"].text
				end
			}
		end
	elsif
		hit += "no-herd"
	end
	hit += ") "
	metadata.elements.each("pkgmetadata/maintainer/email") { |m| hit += m.text + " "}
	puts hit

}


