# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit cmake-utils

DESCRIPTION="Akonadi resources for Google Calendar and Google Contacts."
HOMEPAGE="http://code.google.com/p/libgcal/"
SRC_URI="http://libgcal.googlecode.com/files/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="net-libs/libgcal"
RDEPEND="${DEPEND}"
