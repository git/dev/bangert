# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit python

DESCRIPTION="Funtoo boot-update provides a unified mechanism for configuring the GRUB boot loaders."
HOMEPAGE="http://www.funtoo.org/en/funtoo/core/boot/"
SRC_URI="http://www.funtoo.org/archive/${PN}/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	dev-lang/python
	sys-boot/grub"

src_install() {
	dosbin sbin/boot-update

	insinto $(python_get_sitedir)
	doins -r python/modules/*

	insinto /etc
	newins etc/boot.conf.example boot.conf

	doman doc/boot.conf.5 doc/boot-update.8
	dodoc doc/boot.conf.5.rst doc/boot-update.8.rst

	dodoc README.rst TODO etc/boot.conf.example
}
