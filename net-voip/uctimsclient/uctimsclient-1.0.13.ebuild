# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="IMS client"
HOMEPAGE="http://uctimsclient.berlios.de/"
SRC_URI="mirror://berlios/${PN}/${PN}${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="net-libs/libosip
	net-libs/libeXosip
	net-misc/curl
	dev-libs/libxml2
	x11-libs/gtk+
	media-libs/gstreamer
	media-video/vlc"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

S="${WORKDIR}/${PN}"

src_install() {
	dobin uctimsclient || die
	dodoc README
}
