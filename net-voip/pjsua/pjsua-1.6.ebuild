# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"

DESCRIPTION="pjsua is an open source command line SIP user agent"
HOMEPAGE="http://www.pjsip.org/pjsua.htm"
SRC_URI="http://www.pjsip.org/release/${PV}/pjproject-${PV}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="ipv6 portaudio ssl"

DEPEND="media-libs/speex
	portaudio? ( media-libs/portaudio )
	ssl? ( dev-libs/openssl )"
RDEPEND="${DEPEND}"

S="${WORKDIR}/pjproject-${PV}"

src_configure() {
	local localconf

	localconf='--with-external-speex '
	use portaudio && \
		localconf="${localconf} --with-external-pa"
	use portaudio || \
		localconf="${localconf} --without-pa"

	econf ${localconf} || die

	use ssl && \
		echo '#define PJSIP_HAS_TLS_TRANSPORT 1' >> \
			pjlib/include/pj/config_site.h

	use ipv6 && \
		echo '#define PJ_HAS_IPV6 1' >> \
			pjlib/include/pj/config_site.h

	echo '#define PJSIP_MAX_PKT_LEN 4096' >> \
		pjlib/include/pj/config_site.h

	echo '#define PJSIP_HAS_DIGEST_AKA_AUTH 1' >> \
		pjlib/include/pj/config_site.h
}

src_compile() {
	#fails on parallel build
	emake -j1 || die "Make failed!"
}

src_install() {
	exeinto /usr/bin
	newexe pjsip-apps/bin/pjsua* pjsua
}
