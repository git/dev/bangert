# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"

DESCRIPTION="QjSimple is a qt4 simple cross-platform SIP Client. Based on pjsip."
HOMEPAGE="http://www.ipcom.at/index.php?id=560"
SRC_URI="http://enum.at/fileadmin/ipcom/files/QjSimple-${PV}-src.zip
http://www.pjsip.org/release/1.6/pjproject-1.6.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="media-libs/speex
	dev-libs/openssl
	media-libs/portaudio
	x11-libs/qt-core
	x11-libs/qt-gui"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_configure() {
	local localconf

	cd "${S}"/pjproject-1.6

	localconf='--with-external-speex '
	localconf="${localconf} --with-external-pa"

	econf ${localconf} || die

	echo '#define PJSIP_HAS_TLS_TRANSPORT 1' >> \
		pjlib/include/pj/config_site.h

	echo '#define PJ_HAS_IPV6 1' >> \
		pjlib/include/pj/config_site.h

	target=`grep "export TARGET_NAME" build.mak | awk '{ print $NF}'`

	cd "${S}"/QjSimple-${PV}-src

	sed -i "s/-lspeex-i686-pc-linux-gnu/-lspeex -lspeexdsp/" QjSimple.pro
	sed -i "s/-lportaudio-i686-pc-linux-gnu/-lportaudio/" QjSimple.pro
	sed -i "s/i686-pc-linux-gnu/${target}/" QjSimple.pro
	sed -i "s/pjproject-1.3/pjproject-1.6/" QjSimple.pro

	qmake
}

src_compile() {
	cd "${S}"/pjproject-1.6
	#fails on parallel build
	emake -j1 || die "Make failed!"
	cd "${S}"/QjSimple-${PV}-src
	emake || die "Make failed!"
}

src_install() {
	cd QjSimple-${PV}-src
	exeinto /usr/bin
	newexe QjSimple qjsimple
}
