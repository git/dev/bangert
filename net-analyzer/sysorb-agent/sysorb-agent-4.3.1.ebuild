# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="A agent for SysOrb Monitoring System used to checkin to the server."
HOMEPAGE="http://www.evalesco.com/"
SRC_URI="ftp://ftp.sysorb.com/3.4.1-4333/sysorb-agent-3.4.1-4333.debian22.ia32.deb"

RESTRICT="fetch"

LICENSE="SysorbAgent"
SLOT="0"
KEYWORDS="~x86 -*"
IUSE=""

DEPEND=""
RDEPEND=""

src_unpack() {
	ar x "${DISTDIR}/${A}" || die "Source ${A} not found!"
	mkdir "${S}"
	cd "${S}"
	tar xf ../data.tar.gz || die "data.tar.gz not found in deb"
}

src_install() {
	dosbin usr/sbin/soagent
	dosbin usr/sbin/sysorb-testconf

	#install init.d script
	newinitd "${FILESDIR}/soagent.initd" soagent
	newconfd "${FILESDIR}/soagent.confd" soagent

	insinto /etc/logrotate.d
	doins etc/logrotate.d/soagent

	dodir /var/log/sysorb
	fowners sysorb /var/log/sysorb

	dodir /etc/sysorb
	insinto /etc/sysorb
	doins etc/sysorb/action.conf.sample
	doins etc/sysorb/agent.conf
	doins etc/sysorb/custom.conf.sample
	doins etc/sysorb/log.conf

	fowners sysorb /etc/sysorb \
		/etc/sysorb/action.conf.sample \
		/etc/sysorb/agent.conf \
		/etc/sysorb/custom.conf.sample \
		/etc/sysorb/log.conf

	#TODO man pages
}

pkg_setup() {
	enewgroup sysorb
	enewuser sysorb -1 -1 -1 sysorb
}
