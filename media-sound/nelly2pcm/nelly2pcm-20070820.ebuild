# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Decodes Nellymoser Asao encoded audio in FLV files into WAV"
HOMEPAGE="http://code.google.com/p/nelly2pcm/"
SRC_URI="http://nelly2pcm.googlecode.com/files/${PN}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND=""

S="${WORKDIR}/${PN}"

src_install() {
	dobin nelly2pcm
}
