# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="gst123 is a flexible CLI player in the spirit of ogg123 and mpg123, based on gstreamer."
HOMEPAGE="http://space.twc.de/~stefan/gst123.php"
SRC_URI="http://space.twc.de/~stefan/${PN}/${P}.tar.bz2"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="sys-libs/ncurses
	media-libs/gstreamer
	media-plugins/gst-plugins-ogg
	media-plugins/gst-plugins-vorbis
	|| (
		media-plugins/gst-plugins-mad
		media-plugins/gst-plugins-twolame
		media-plugins/gst-plugins-lame
	)
	"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
	dodoc README TODO NEWS || die
}
