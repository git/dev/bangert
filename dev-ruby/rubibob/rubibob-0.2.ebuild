# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="RuBiBoB enables you to send sms and check your balance of your BiBoB account."
HOMEPAGE="http://sourceforge.net/projects/rubibob/"
SRC_URI="mirror://sourceforge/rubibob/RuBiBoB.v${PV}.src.tgz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="dev-ruby/highline
	ruby-gem/soap4r"

S=${WORKDIR}/RuBiBoB.v${PV}

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}"/fix-include-path.patch
}

src_install() {
	newbin src/RuBiBoB.rb rubibob

	insinto /usr/lib/ruby/rubibob
	doins src/default.rb src/defaultDriver.rb \
		src/defaultMappingRegistry.rb src/bibobFunctions.rb \
		src/BiBoB_x0020_SMSClient.rb

	dodoc README.txt
}
